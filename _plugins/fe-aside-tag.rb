module Jekyll
    class FeAsideTag < Liquid::Tag
  
      def initialize(tag_name, text, tokens)
        super
        @text = text
      end
  
      def render(context)
        %Q[<span class="aside">#{@text}</span>]
      end
    end
  end
  
  Liquid::Template.register_tag('aside', Jekyll::FeAsideTag)
