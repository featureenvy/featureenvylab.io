---
category: security
tags: linux shell
---

[Pentestmonkey Reverse Shell Cheat Sheet](http://pentestmonkey.net/cheat-sheet/shells/reverse-shell-cheat-sheet) contains a large number of possible reverse shells.
