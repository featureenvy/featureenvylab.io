---
category: security
tags: linux enum
---

[SUID3NUM](https://github.com/Anon-Exploiter/SUID3NUM) can be used to find SUID binaries. It also shows uncommon SUID binaries and possible exploits.
