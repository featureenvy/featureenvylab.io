function handleCodeTabs(codeTab) {
    // Add code switcher
    var allTabs = codeTab.querySelectorAll("div.code-listing")

    var tabContainer = document.createElement("div");
    tabContainer.classList.add("listing-header");

    let isFirst = true;
    allTabs.forEach(function (tab) {
        var span = document.createElement("span");
        span.textContent = tab.getAttribute("data-name");
        if (isFirst) {
            span.classList.add("code-selected");

            isFirst = false;
        }

        tabContainer.append(span);
    });

    codeTab.insertBefore(tabContainer, codeTab.firstChild);


    // select first code listing
    var codeTab = document.getElementsByClassName("code-tabs")[0];
    codeTab.querySelector(".code-listing").classList.add("code-selected");

    // add on click event listener
    var codeTab = document.getElementsByClassName("code-tabs")[0];
    codeTab.querySelectorAll(".listing-header span").forEach(function (switcher) {
        switcher.onclick = function () {
            // switch code listing
            codeTab.querySelectorAll(".code-listing").forEach(function (listing) {
                listing.classList.remove("code-selected");
            });

            codeTab.querySelector(".code-listing[data-name=" + switcher.textContent + "]").classList.add("code-selected");

            // switch selected tab
            codeTab.querySelectorAll(".listing-header span").forEach(function (tab) {
                tab.classList.remove("code-selected");
            });

            switcher.classList.add("code-selected");
        };
    });
}

function handleAllCodeTabs() {
    Array.from(document.getElementsByClassName("code-tabs")).forEach(function (codeTab) {
        handleCodeTabs(codeTab);
    })
}

document.addEventListener('DOMContentLoaded', handleAllCodeTabs, false);

function handleAnimationList(animationList) {
    var animationItems = animationList.querySelectorAll(".animation-item");
    var frames = [];

    function selectAnimation(i) {
        animationItems.forEach(function (animation) {
            animation.classList.remove("animation-selected");
        });

        animationItems[i].classList.add("animation-selected");

        frames.forEach(function (frame) {
            frame.classList.remove("code-selected");
        });

        frames[i].classList.add("code-selected");
    }

    // auto-forward
    var selected = 0;
    animationItems[selected].classList.add("animation-selected");
    function nextItem() {
        selected = (selected + 1) % animationItems.length;
        selectAnimation(selected);
    }

    var interval = setInterval(nextItem, 2000);

    // play controls
    var controls = document.createElement("div");
    controls.classList.add("listing-header");

    var play = document.createElement("span");
    play.classList.add("fas", "fa-pause");
    controls.append(play);

    play.onclick = function () {
        if (play.classList.contains("fa-play")) {
            play.classList.remove("fa-play");
            play.classList.add("fa-pause");

            interval = setInterval(nextItem, 2000);
        } else {
            play.classList.remove("fa-pause");
            play.classList.add("fa-play");

            clearInterval(interval);
        }
    };

    for (var i = 0; i < animationItems.length; i++) {
        var frame = document.createElement("span");
        frame.textContent = (i + 1);
        frames.push(frame);

        (function (n) {
            frame.onclick = function () {
                play.classList.remove("fa-pause");
                play.classList.add("fa-play");

                clearInterval(interval);

                selected = n;
                selectAnimation(n);
            }
        }(i));

        controls.append(frame);
    };

    animationList.parentElement.insertBefore(controls, animationList);
}

function handleAllAnimationLists() {
    Array.from(document.getElementsByClassName("animation-list")).forEach(function (animation) {
        handleAnimationList(animation);
    })
}

document.addEventListener('DOMContentLoaded', handleAllAnimationLists, false);
